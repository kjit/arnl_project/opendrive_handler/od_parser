#pragma once
#include "Lane.h"
#include "XmlNode.h"

#include <map>
#include <string>
#include <vector>

namespace odr
{

struct LaneSection : public XmlNode
{
    LaneSection(std::string road_id, double s0);

    std::vector<Lane> get_lanes() const;

    int  get_lane_id(const double s, const double t) const;
    Lane get_lane(const double s, const double t) const;

    // Overload the equality operator
    bool operator==(const LaneSection& other) const
    {
        return road_id == other.road_id && s0 == other.s0;
    }

    bool operator!=(const LaneSection& other) const { return !(*this == other); }

    std::string         road_id = "";
    double              s0 = 0;
    std::map<int, Lane> id_to_lane;
};

} // namespace odr
