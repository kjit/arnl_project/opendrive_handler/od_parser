#pragma once
#include "Math.hpp"
#include "RoadGeometry.h"

#include <memory>
#include <set>

namespace odr
{

struct Arc : public RoadGeometry
{
    Arc(double s0, double x0, double y0, double hdg0, double length, double curvature);

    std::unique_ptr<RoadGeometry> clone() const override;

    Vec2D get_xy(double s) const override;
    double get_s(double x, double y) const override;
    Vec2D get_grad(double s) const override;

    std::set<double> approximate_linear(double eps) const override;

    double curvature = 0;

private:
    Vec2D midpoint(const Vec2D& p1, const Vec2D& p2) const;
    double slope(const Vec2D& p1, const Vec2D& p2) const;
    Vec2D circleCenter(const Vec2D& p1, const Vec2D& p2, const Vec2D& p3) const;
    double circleRadius(const Vec2D& center, const Vec2D& point) const;
    Vec2D closestPointOnCircle(const Vec2D& center, double radius, const Vec2D& P) const;
    double calculateAngle(const Vec2D& center, const Vec2D& point) const;
    double distanceToArc(const Vec2D& center, double radius, const Vec2D& Q, const Vec2D& A, const Vec2D& B) const;
};

} // namespace odr