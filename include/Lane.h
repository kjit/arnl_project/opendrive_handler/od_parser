#pragma once
#include "Geometries/CubicSpline.h"
#include "RoadMark.h"
#include "XmlNode.h"

#include <cstddef>
#include <functional>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>
#include <iostream>

namespace odr
{

struct HeightOffset
{
    HeightOffset(double inner, double outer);

    double inner = 0;
    double outer = 0;
};

struct LaneKey
{
    LaneKey() = default;
    LaneKey(std::string road_id, double lanesection_s0, int lane_id);
    std::string to_string() const;

    std::string road_id = "";
    double      lanesection_s0 = 0;
    int         lane_id = 0;

    // Overload the equality operator
    bool operator==(const LaneKey& other) const
    {
        return road_id == other.road_id && lanesection_s0 == other.lanesection_s0 && lane_id == other.lane_id;
    }

    bool operator!=(const LaneKey& other) const { return !(*this == other); }

    bool lane_change(const LaneKey& other) const
    {
        return this->road_id == other.road_id && this->lanesection_s0 == other.lanesection_s0;
    }

    friend std::ostream& operator<<(std::ostream& os, const LaneKey& lane_key)
    {
        os << "(" << lane_key.road_id << ", " << lane_key.lanesection_s0 << ", " << lane_key.lane_id << ")";
        return os;
    }
};

struct Lane : public XmlNode
{
    Lane(std::string road_id, double lanesection_s0, int id, bool level, std::string type);

    std::vector<RoadMark> get_roadmarks(const double s_start, const double s_end) const;
    bool has_lanechange() const
    {
        return right_lane.road_id != "" || left_lane.road_id != "";
    }

    LaneKey     key;
    int         id;
    bool        level = false;
    int         predecessor = 0;
    int         successor = 0;
    std::string type = "";

    CubicSpline lane_width;
    CubicSpline outer_border;
    CubicSpline inner_border;

    std::map<double, HeightOffset> s_to_height_offset;
    std::set<RoadMarkGroup>        roadmark_groups;

    LaneKey right_lane;
    LaneKey left_lane;
};

} // namespace odr

namespace std
{
template<>
struct hash<odr::LaneKey>
{
    size_t operator()(const odr::LaneKey& key) const
    {
        return ((hash<string>()(key.road_id) ^ (hash<double>()(key.lanesection_s0) << 1)) >> 1) ^ (hash<int>()(key.lane_id) << 1);
    }
};

template<>
struct equal_to<odr::LaneKey>
{
    bool operator()(const odr::LaneKey& lhs, const odr::LaneKey& rhs) const
    {
        return (lhs.road_id == rhs.road_id) && (lhs.lanesection_s0 == rhs.lanesection_s0) && (lhs.lane_id == rhs.lane_id);
    }
};

template<>
struct less<odr::LaneKey>
{
    bool operator()(const odr::LaneKey& lhs, const odr::LaneKey& rhs) const
    {
        if (lhs.road_id != rhs.road_id)
            return lhs.road_id < rhs.road_id;
        if (lhs.lanesection_s0 != rhs.lanesection_s0)
            return lhs.lanesection_s0 < rhs.lanesection_s0;
        if (lhs.lane_id != rhs.lane_id)
            return lhs.lane_id < rhs.lane_id;
        return false;
    }
};
} // namespace std