#pragma once
#include "Junction.h"
#include "Road.h"
#include "RoadNetworkMesh.h"
#include "RoutingGraph.h"

#include <pugixml/pugixml.hpp>

#include <map>
#include <string>
#include <climits>
#include <vector>

using namespace std;

namespace odr
{

class OpenDriveMap
{
public:
    OpenDriveMap(const string& xodr_file,
                 const bool         center_map = false,
                 const bool         with_road_objects = true,
                 const bool         with_lateral_profile = true,
                 const bool         with_lane_height = true,
                 const bool         abs_z_for_for_local_road_obj_outline = false,
                 const bool         fix_spiral_edge_cases = true,
                 const bool         with_road_signals = true);

    void connect_lanechanges();

    vector<Road>     get_roads() const;
    vector<Junction> get_junctions() const;

    RoadNetworkMesh get_road_network_mesh(const double eps) const;
    RoutingGraph    get_routing_graph() const;
    std::vector<std::tuple<LaneKey, LaneKey, LaneKey>> get_junction_connections(const JunctionConnection& conn, const std::string& id_junc) const;
    std::vector<std::vector<LaneKey>> shortest_path(const LaneKey& start, const LaneKey& end) const;
    std::vector<LaneKey> get_right_lanes(const LaneKey& lane) const;
    std::vector<LaneKey> get_left_lanes(const LaneKey& lane) const;

    std::vector<LaneKey> find_lane_predecessors(const Lane& lane) const;
    std::vector<LaneKey> find_lane_successors(const Lane& lane) const;
    std::pair<LaneKey, LaneKey> find_lane_adjacents(const Lane& lane) const;
    std::vector<LaneKey> get_junction_connections(const Lane& lane) const;

    std::tuple<LaneKey, double> find_lane(double x, double y, double eps=0.1) const;
    /**
     * Older version don't use only for benchmarking
     */
    std::tuple<LaneKey, double> match_lane(double x, double y, double eps=0.1) const;

    string proj4 = "";
    double x_offs = 0;
    double y_offs = 0;
    const string  xodr_file = "";
    pugi::xml_document xml_doc;
    RoutingGraph routing_graph;

    map<string, Road> id_to_road;
    map<string, Junction> id_to_junction;
};

} // namespace odr