#include "Geometries/Line.h"
#include "Geometries/RoadGeometry.h"
#include "Math.hpp"

#include <cmath>
#include <iostream>

namespace odr
{

Line::Line(double s0, double x0, double y0, double hdg0, double length) : RoadGeometry(s0, x0, y0, hdg0, length, GeometryType_Line) {}

std::unique_ptr<RoadGeometry> Line::clone() const { return std::make_unique<Line>(*this); }

Vec2D Line::get_xy(double s) const
{
    const double x = (std::cos(hdg0) * (s - s0)) + x0;
    const double y = (std::sin(hdg0) * (s - s0)) + y0;
    return Vec2D{x, y};
}

double Line::get_s(double x, double y) const
{
    //std::cout << "line get_s function" << std::endl;

    Vec2D A = this->get_xy(this->s0);
    Vec2D B = this->get_xy(this->s0 + this->length);
    Vec2D P = {x, y};

    Vec2D AB = odr::sub(B, A);
    Vec2D AP = odr::sub(P, A);

    double proj = std::inner_product(AP.begin(), AP.end(), AB.begin(), 0.0);

    double len_squared = (A[0] - B[0]) * (A[0] - B[0]) + (A[1] - B[1]) * (A[1] - B[1]);

    double d = proj / len_squared;

    //std::cout << "get_s line A: " << A[0] << "," << A[1] << " B: " << B[0] << "," << B[1] << " P: " << P[0] << "," << P[1] << std::endl;
    //std::cout << "get_s line AB: " << AB[0] << ";" << AB[1] << " AP: " << AP[0] << ";" << AP[1] << std::endl;
    //std::cout << "get_s line d: " << d << " proj: " << proj << " len: " << len_squared << std::endl;

    if (d <= 0.0 || d >= 1.0) {
        //std::cout << "point not on line" << std::endl;
        return -1;
    }
        
    double s = this->length * d;

    //std::cout << "get_s line s: " << s << " d: " << d << std::endl;

    return this->s0 + s;
}

Vec2D Line::get_grad(double s) const { return {{std::cos(hdg0), std::sin(hdg0)}}; }

std::set<double> Line::approximate_linear(double eps) const
{
    if (eps > this->length)
        return {this->s0, this->s0 + this->length};

    std::set<double> s_vals;
    for (double s = this->s0; s < (this->s0 + this->length); s += eps)
        s_vals.insert(s);

    return s_vals;
}

} // namespace odr
