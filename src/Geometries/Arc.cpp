#include "Geometries/Arc.h"
#include "Geometries/RoadGeometry.h"

#include <cmath>
#include <iostream>

namespace odr
{
Arc::Arc(double s0, double x0, double y0, double hdg0, double length, double curvature) :
    RoadGeometry(s0, x0, y0, hdg0, length, GeometryType_Arc), curvature(curvature)
{
}

std::unique_ptr<RoadGeometry> Arc::clone() const { return std::make_unique<Arc>(*this); }

Vec2D Arc::get_xy(double s) const
{
    const double angle_at_s = (s - s0) * curvature - M_PI / 2;
    const double r = 1 / curvature;
    const double xs = r * (std::cos(hdg0 + angle_at_s) - std::sin(hdg0)) + x0;
    const double ys = r * (std::sin(hdg0 + angle_at_s) + std::cos(hdg0)) + y0;
    return Vec2D{xs, ys};
}

double Arc::get_s(double x, double y) const
{
    const Vec2D A = this->get_xy(this->s0);
    const Vec2D M = this->get_xy(this->s0 + this->length / 2);
    const Vec2D B = this->get_xy(this->s0 + this->length);
    const Vec2D P = {x, y};

    Vec2D C = circleCenter(A, M, B);
    double radius = circleRadius(C, A);
    Vec2D Q = closestPointOnCircle(C, radius, P);

    //std::cout << "get_s arc A: " << A[0] << ";" << A[1] << " M: " << M[0] << ";" << M[1] << " B: " << B[0] << ";" << B[1] << std::endl;
    //std::cout << "get_s arc Q: " << Q[0] << ";" << Q[1] << " P: " << P[0] << ";" << P[1] << " C: " << C[0] << ";" << C[1] << std::endl;

    if (euclDistance(P, Q) > 20.0) // this is not the best hardcoded
        return -1;

    double s = distanceToArc(C, radius, Q, A, B);

    if (s == -1)
        return -1;

    //std::cout << "get_s arc s: " << s << std::endl;

    return this->s0 + s;
}

Vec2D Arc::get_grad(double s) const
{
    const double dx = std::sin((M_PI / 2) - curvature * (s - s0) - hdg0);
    const double dy = std::cos((M_PI / 2) - curvature * (s - s0) - hdg0);
    return {{dx, dy}};
}

std::set<double> Arc::approximate_linear(double eps) const
{
    if (eps > this->length)
        return {this->s0, this->s0 + this->length};
    
    //const double s_step = eps / std::abs(this->curvature); // sample at approx. every 1° when eps is 0.01 | currently not used
    std::set<double> s_vals;
    for (double s = s0; s < (s0 + length); s += eps)
        s_vals.insert(s);
    s_vals.insert(s0 + length);

    return s_vals;
}

Vec2D Arc::midpoint(const Vec2D& p1, const Vec2D& p2) const {
    return { (p1[0] + p2[0]) / 2.0, (p1[1] + p2[1]) / 2.0 };
}
 
// Function to calculate the slope of the line connecting two points
double Arc::slope(const Vec2D& p1, const Vec2D& p2) const {
    if (p2[0] == p1[0]) return std::numeric_limits<double>::infinity(); // vertical line
    return (p2[1] - p1[1]) / (p2[0] - p1[0]);
}
 
// Function to find the center of the circle given three points
Vec2D Arc::circleCenter(const Vec2D& p1, const Vec2D& p2, const Vec2D& p3) const {
    // Midpoints of the segments
    auto mid1 = midpoint(p1, p2);
    auto mid2 = midpoint(p2, p3);
    // Slopes of the segments
    double slope1 = slope(p1, p2);
    double slope2 = slope(p2, p3);
    // Perpendicular slopes
    double perpSlope1 = (slope1 == 0) ? std::numeric_limits<double>::infinity() : -1.0 / slope1;
    double perpSlope2 = (slope2 == 0) ? std::numeric_limits<double>::infinity() : -1.0 / slope2;
 
    // Line equations: y - y1 = m(x - x1)
    // For first perpendicular bisector
    double c1 = mid1[1] - perpSlope1 * mid1[0]; // y-intercept for the first bisector
 
    // For second perpendicular bisector
    double c2 = mid2[1] - perpSlope2 * mid2[0]; // y-intercept for the second bisector
 
    // Calculate the center of the circle
    double centerX, centerY;
 
    if (perpSlope1 == std::numeric_limits<double>::infinity()) {
        // First line is vertical
        centerX = mid1[0];
        centerY = perpSlope2 * centerX + c2;
    } else if (perpSlope2 == std::numeric_limits<double>::infinity()) {
        // Second line is vertical
        centerX = mid2[0];
        centerY = perpSlope1 * centerX + c1;
    } else {
        // Solve for intersection of the two lines
        centerX = (c2 - c1) / (perpSlope1 - perpSlope2);
        centerY = perpSlope1 * centerX + c1;
    }
 
    return { centerX, centerY };
}
 
double Arc::circleRadius(const Vec2D& center, const Vec2D& point) const {
    return std::sqrt(std::pow(point[0] - center[0], 2) + std::pow(point[1] - center[1], 2));
}
 
Vec2D Arc::closestPointOnCircle(const Vec2D& center, double radius, const Vec2D& P) const {
    // Vector from center to P
    double vectorX = P[0] - center[0];
    double vectorY = P[1] - center[1];
 
    // Calculate the distance from the center to point P
    double distance = std::sqrt(vectorX * vectorX + vectorY * vectorY);
 
    // If the distance is zero, P is at the center of the circle; return a point on the circumference
    if (distance == 0) {
        return { center[0] + radius, center[1] }; // Arbitrary point on the circumference
    }
 
    // Normalize the vector to find the direction
    double normalizedX = vectorX / distance;
    double normalizedY = vectorY / distance;
 
    // Calculate the closest point on the circle
    double closestX = center[0] + normalizedX * radius;
    double closestY = center[1] + normalizedY * radius;
 
    return { closestX, closestY };
}
 
double Arc::calculateAngle(const Vec2D& center, const Vec2D& point) const {
    return std::atan2(point[1] - center[1], point[0] - center[0]);
}
 
double Arc::distanceToArc(const Vec2D& center, double radius, const Vec2D& Q, const Vec2D& A, const Vec2D& B) const {
    // Calculate angles
    double angleQ = calculateAngle(center, Q);
    double angleA = calculateAngle(center, A);
    double angleB = calculateAngle(center, B);
 
    // Normalize angles to the range [0, 2π]
    angleQ = fmod(angleQ + 2 * M_PI, 2 * M_PI);
    angleA = fmod(angleA + 2 * M_PI, 2 * M_PI);
    angleB = fmod(angleB + 2 * M_PI, 2 * M_PI);

    bool isClockwise = fmod(angleB - angleA + 2 * M_PI, 2 * M_PI) < M_PI;

    // Check if Q is between start and end based on the arc direction
    bool isQBetween;
    if (isClockwise) {
        // Clockwise arc: Q is between start and end if angleQ is within [angleStart, angleEnd]
        isQBetween = fmod(angleQ - angleA + 2 * M_PI, 2 * M_PI) <= fmod(angleB - angleA + 2 * M_PI, 2 * M_PI);
    } else {
        // Counterclockwise arc: Q is between start and end if angleQ is within [angleEnd, angleStart]
        isQBetween = fmod(angleA - angleQ + 2 * M_PI, 2 * M_PI) <= fmod(angleA - angleB + 2 * M_PI, 2 * M_PI);
    }

    //std::cout << "get_s distToArc angleQ: " << angleQ << " angleA: " << angleA << " angleB: " << angleB << std::endl;
    //std::cout << "get_s distToArc isClockwise: " << isClockwise << " isQBetween: " << isQBetween << std::endl;
    
    if (isQBetween) {
        // Calculate arc length from start to Q
        double arcDistanceToQ;
        if (isClockwise) {
            arcDistanceToQ = fmod(angleQ - angleA + 2 * M_PI, 2 * M_PI) * radius;
        } else {
            arcDistanceToQ = fmod(angleA - angleQ + 2 * M_PI, 2 * M_PI) * radius;
        }
        return arcDistanceToQ;
    } else
        return -1;
}

} // namespace odr
