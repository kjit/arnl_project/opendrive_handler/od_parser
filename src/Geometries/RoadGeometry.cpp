#include "Geometries/RoadGeometry.h"
#include "Utils.hpp"

#include <iostream>

namespace odr
{

RoadGeometry::RoadGeometry(double s0, double x0, double y0, double hdg0, double length, GeometryType type) :
    s0(s0), x0(x0), y0(y0), hdg0(hdg0), length(length), type(type)
{
}

double RoadGeometry::get_s(double x, double y) const
{
    double eps = 1e-2;
    std::function<double(double)> f_dist = [&](const double s)
    {
        const Vec2D pt = this->get_xy(s);
        return euclDistance(Vec2D{pt[0], pt[1]}, {x, y});
    };
    double closest_s = golden_section_search<double>(f_dist, this->s0, this->s0 + length, eps);
    
    //std::cout << "Parser geom get_s function" << std::endl;

    if (std::abs(closest_s - this->s0) <= eps || std::abs(closest_s - (this->s0 + length)) <= eps) {
        return -1;
    }

    return closest_s;
}

} // namespace odr
