#include "OpenDriveMap.h"
#include "Geometries/Arc.h"
#include "Geometries/CubicSpline.h"
#include "Geometries/Line.h"
#include "Geometries/ParamPoly3.h"
#include "Geometries/RoadGeometry.h"
#include "Geometries/Spiral.h"
#include "Junction.h"
#include "Lane.h"
#include "LaneSection.h"
#include "LaneValidityRecord.h"
#include "Math.hpp"
#include "RefLine.h"
#include "Road.h"
#include "RoadMark.h"
#include "RoadObject.h"
#include "RoadSignal.h"
#include "Utils.hpp"

#include <algorithm>
#include <climits>
#include <cmath>
#include <iterator>
#include <memory>
#include <set>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>
#include <iostream>
#include <chrono>

namespace odr
{
vector<LaneValidityRecord> extract_lane_validity_records(const pugi::xml_node& xml_node)
{
    vector<LaneValidityRecord> lane_validities;
    for (const auto& validity_node : xml_node.children("validity"))
    {
        LaneValidityRecord lane_validity{validity_node.attribute("fromLane").as_int(INT_MIN), validity_node.attribute("toLane").as_int(INT_MAX)};
        lane_validity.xml_node = validity_node;

        // fromLane should not be greater than toLane, since the standard defines them as follows:
        // fromLane - the minimum ID of lanes for which the object is valid
        // toLane - the maximum ID of lanes for which the object is valid
        // If we find such a violation, we set both IDs to 0 which means the
        // object is only applicable to the centerlane.
        CHECK_AND_REPAIR(
            lane_validity.from_lane <= lane_validity.to_lane, "lane_validity::from_lane > lane_validity.to_lane", lane_validity.from_lane = 0;
            lane_validity.to_lane = 0)

        lane_validities.push_back(move(lane_validity));
    }
    return lane_validities;
}

OpenDriveMap::OpenDriveMap(const string& xodr_file,
                           const bool         center_map,
                           const bool         with_road_objects,
                           const bool         with_lateral_profile,
                           const bool         with_lane_height,
                           const bool         abs_z_for_for_local_road_obj_outline,
                           const bool         fix_spiral_edge_cases,
                           const bool         with_road_signals) :
    xodr_file(xodr_file)
{
    pugi::xml_parse_result result = this->xml_doc.load_file(xodr_file.c_str());
    if (!result)
        printf("%s\n", result.description());

    pugi::xml_node odr_node = this->xml_doc.child("OpenDRIVE");

    if (auto geoReference_node = odr_node.child("header").child("geoReference"))
        this->proj4 = geoReference_node.text().as_string("");

    size_t cnt = 1;
    if (center_map)
    {
        for (pugi::xml_node road_node : odr_node.children("road"))
        {
            for (pugi::xml_node geometry_hdr_node : road_node.child("planView").children("geometry"))
            {
                const double x0 = geometry_hdr_node.attribute("x").as_double(0.0);
                this->x_offs = this->x_offs + ((x0 - this->x_offs) / cnt);
                const double y0 = geometry_hdr_node.attribute("y").as_double(0.0);
                this->y_offs = this->y_offs + ((y0 - this->y_offs) / cnt);
                cnt++;
            }
        }
    }

    for (pugi::xml_node junction_node : odr_node.children("junction"))
    {
        /* make junction */
        const string junction_id = junction_node.attribute("id").as_string("");

        Junction& junction =
            this->id_to_junction.insert({junction_id, Junction(junction_node.attribute("name").as_string(""), junction_id)}).first->second;
        junction.xml_node = junction_node;

        for (pugi::xml_node connection_node : junction_node.children("connection"))
        {
            string contact_point_str = connection_node.attribute("contactPoint").as_string("");
            CHECK_AND_REPAIR(contact_point_str == "start" || contact_point_str == "end",
                             "Junction::Connection::contactPoint invalid value",
                             contact_point_str = "start"); // default to start
            const JunctionConnection::ContactPoint junction_conn_contact_point =
                (contact_point_str == "start") ? JunctionConnection::ContactPoint_Start : JunctionConnection::ContactPoint_End;

            const string   junction_connection_id = connection_node.attribute("id").as_string("");
            JunctionConnection& junction_connection = junction.id_to_connection
                                                          .insert({junction_connection_id,
                                                                   JunctionConnection(junction_connection_id,
                                                                                      connection_node.attribute("incomingRoad").as_string(""),
                                                                                      connection_node.attribute("connectingRoad").as_string(""),
                                                                                      junction_conn_contact_point)})
                                                          .first->second;

            for (pugi::xml_node lane_link_node : connection_node.children("laneLink"))
            {
                JunctionLaneLink lane_link(lane_link_node.attribute("from").as_int(0), lane_link_node.attribute("to").as_int(0));
                junction_connection.lane_links.insert(lane_link);
            }
        }

        const size_t num_conns = junction.id_to_connection.size();
        CHECK(num_conns > 0, "Junction::connections == 0");
        if (num_conns < 1)
            continue;

        for (pugi::xml_node priority_node : junction_node.children("priority"))
        {
            JunctionPriority junction_priority(priority_node.attribute("high").as_string(""), priority_node.attribute("low").as_string(""));
            junction.priorities.insert(junction_priority);
        }

        for (pugi::xml_node controller_node : junction_node.children("controller"))
        {
            const string junction_controller_id = controller_node.attribute("id").as_string("");
            junction.id_to_controller.insert({junction_controller_id,
                                              JunctionController(junction_controller_id,
                                                                 controller_node.attribute("type").as_string(""),
                                                                 controller_node.attribute("sequence").as_uint(0))});
        }
    }

    for (pugi::xml_node road_node : odr_node.children("road"))
    {
        /* make road */
        string road_id = road_node.attribute("id").as_string("");
        CHECK_AND_REPAIR(this->id_to_road.find(road_id) == this->id_to_road.end(),
                         (string("road::id already exists - ") + road_id).c_str(),
                         road_id = road_id + string("_dup"));

        string rule_str = string(road_node.attribute("rule").as_string("RHT"));
        transform(rule_str.begin(), rule_str.end(), rule_str.begin(), [](unsigned char c) { return tolower(c); });
        const bool is_left_hand_traffic = (rule_str == "lht");

        Road& road = this->id_to_road
                         .insert({road_id,
                                  Road(road_id,
                                       road_node.attribute("length").as_double(0.0),
                                       road_node.attribute("junction").as_string(""),
                                       road_node.attribute("name").as_string(""),
                                       is_left_hand_traffic)})
                         .first->second;
        road.xml_node = road_node;

        CHECK_AND_REPAIR(road.length >= 0, "road::length < 0", road.length = 0);

        /* parse road links */
        for (bool is_predecessor : {true, false})
        {
            pugi::xml_node road_link_node =
                is_predecessor ? road_node.child("link").child("predecessor") : road_node.child("link").child("successor");
            if (road_link_node)
            {
                RoadLink& link = is_predecessor ? road.predecessor : road.successor;
                link.id = road_link_node.attribute("elementId").as_string("");

                string type_str = road_link_node.attribute("elementType").as_string("");
                CHECK_AND_REPAIR(type_str == "road" || type_str == "junction",
                                 "Road::Succ/Predecessor::Link::elementType invalid type",
                                 type_str = "road"); // default to road
                link.type = (type_str == "road") ? RoadLink::Type_Road : RoadLink::Type_Junction;

                if (link.type == RoadLink::Type_Road)
                {
                    // junction connection has no contact point
                    string contact_point_str = road_link_node.attribute("contactPoint").as_string("");
                    CHECK_AND_REPAIR(contact_point_str == "start" || contact_point_str == "end",
                                     "Road::Succ/Predecessor::Link::contactPoint invalid type",
                                     contact_point_str = "start"); // default to start
                    link.contact_point = (contact_point_str == "start") ? RoadLink::ContactPoint_Start : RoadLink::ContactPoint_End;
                }

                link.xml_node = road_link_node;
            }
        }

        /* parse road neighbors */
        for (pugi::xml_node road_neighbor_node : road_node.child("link").children("neighbor"))
        {
            const string road_neighbor_id = road_neighbor_node.attribute("elementId").as_string("");
            const string road_neighbor_side = road_neighbor_node.attribute("side").as_string("");
            const string road_neighbor_direction = road_neighbor_node.attribute("direction").as_string("");
            RoadNeighbor      road_neighbor(road_neighbor_id, road_neighbor_side, road_neighbor_direction);
            road_neighbor.xml_node = road_neighbor_node;
            road.neighbors.push_back(road_neighbor);
        }

        /* parse road type and speed */
        for (pugi::xml_node road_type_node : road_node.children("type"))
        {
            double      s = road_type_node.attribute("s").as_double(0.0);
            string type = road_type_node.attribute("type").as_string("");

            CHECK_AND_REPAIR(s >= 0, "road::type::s < 0", s = 0);

            road.s_to_type[s] = type;
            if (pugi::xml_node node = road_type_node.child("speed"))
            {
                const string speed_record_max = node.attribute("max").as_string("");
                const string speed_record_unit = node.attribute("unit").as_string("");
                SpeedRecord       speed_record(speed_record_max, speed_record_unit);
                speed_record.xml_node = node;
                road.s_to_speed.insert({s, speed_record});
            }
        }

        /* make ref_line - parse road geometries */
        for (pugi::xml_node geometry_hdr_node : road_node.child("planView").children("geometry"))
        {
            double s0 = geometry_hdr_node.attribute("s").as_double(0.0);
            double x0 = geometry_hdr_node.attribute("x").as_double(0.0) - this->x_offs;
            double y0 = geometry_hdr_node.attribute("y").as_double(0.0) - this->y_offs;
            double hdg0 = geometry_hdr_node.attribute("hdg").as_double(0.0);
            double length = geometry_hdr_node.attribute("length").as_double(0.0);

            CHECK_AND_REPAIR(s0 >= 0, "road::planView::geometry::s < 0", s0 = 0);
            CHECK_AND_REPAIR(length >= 0, "road::planView::geometry::length < 0", length = 0);

            pugi::xml_node geometry_node = geometry_hdr_node.first_child();
            string    geometry_type = geometry_node.name();
            if (geometry_type == "line")
            {
                road.ref_line.s0_to_geometry[s0] = make_unique<Line>(s0, x0, y0, hdg0, length);
            }
            else if (geometry_type == "spiral")
            {
                double curv_start = geometry_node.attribute("curvStart").as_double(0.0);
                double curv_end = geometry_node.attribute("curvEnd").as_double(0.0);
                if (!fix_spiral_edge_cases)
                {
                    road.ref_line.s0_to_geometry[s0] = make_unique<Spiral>(s0, x0, y0, hdg0, length, curv_start, curv_end);
                }
                else
                {
                    if (abs(curv_start) < 1e-6 && abs(curv_end) < 1e-6)
                    {
                        // In effect a line
                        road.ref_line.s0_to_geometry[s0] = make_unique<Line>(s0, x0, y0, hdg0, length);
                    }
                    else if (abs(curv_end - curv_start) < 1e-6)
                    {
                        // In effect an arc
                        road.ref_line.s0_to_geometry[s0] = make_unique<Arc>(s0, x0, y0, hdg0, length, curv_start);
                    }
                    else
                    {
                        // True spiral
                        road.ref_line.s0_to_geometry[s0] = make_unique<Spiral>(s0, x0, y0, hdg0, length, curv_start, curv_end);
                    }
                }
            }
            else if (geometry_type == "arc")
            {
                double curvature = geometry_node.attribute("curvature").as_double(0.0);
                road.ref_line.s0_to_geometry[s0] = make_unique<Arc>(s0, x0, y0, hdg0, length, curvature);
            }
            else if (geometry_type == "paramPoly3")
            {
                double aU = geometry_node.attribute("aU").as_double(0.0);
                double bU = geometry_node.attribute("bU").as_double(0.0);
                double cU = geometry_node.attribute("cU").as_double(0.0);
                double dU = geometry_node.attribute("dU").as_double(0.0);
                double aV = geometry_node.attribute("aV").as_double(0.0);
                double bV = geometry_node.attribute("bV").as_double(0.0);
                double cV = geometry_node.attribute("cV").as_double(0.0);
                double dV = geometry_node.attribute("dV").as_double(0.0);

                bool pRange_normalized = true;
                if (geometry_node.attribute("pRange") || geometry_hdr_node.attribute("pRange"))
                {
                    string pRange_str = geometry_node.attribute("pRange") ? geometry_node.attribute("pRange").as_string("")
                                                                               : geometry_hdr_node.attribute("pRange").as_string("");
                    transform(pRange_str.begin(), pRange_str.end(), pRange_str.begin(), [](unsigned char c) { return tolower(c); });
                    if (pRange_str == "arclength")
                        pRange_normalized = false;
                }
                road.ref_line.s0_to_geometry[s0] =
                    make_unique<ParamPoly3>(s0, x0, y0, hdg0, length, aU, bU, cU, dU, aV, bV, cV, dV, pRange_normalized);
            }
            else
            {
                printf("Could not parse %s\n", geometry_type.c_str());
                continue;
            }

            road.ref_line.s0_to_geometry.at(s0)->xml_node = geometry_node;
        }

        map<string /*x path query*/, CubicSpline&> cubic_spline_fields{{".//elevationProfile//elevation", road.ref_line.elevation_profile},
                                                                                 {".//lanes//laneOffset", road.lane_offset}};

        if (with_lateral_profile)
            cubic_spline_fields.insert({".//lateralProfile//superelevation", road.superelevation});

        /* parse elevation profiles, lane offsets, superelevation */
        for (auto entry : cubic_spline_fields)
        {
            /* handle splines not starting at s=0, assume value 0 until start */
            entry.second.s0_to_poly[0.0] = Poly3(0.0, 0.0, 0.0, 0.0, 0.0);

            pugi::xpath_node_set nodes = road_node.select_nodes(entry.first.c_str());
            for (pugi::xpath_node node : nodes)
            {
                double s0 = node.node().attribute("s").as_double(0.0);
                double a = node.node().attribute("a").as_double(0.0);
                double b = node.node().attribute("b").as_double(0.0);
                double c = node.node().attribute("c").as_double(0.0);
                double d = node.node().attribute("d").as_double(0.0);

                CHECK_AND_REPAIR(s0 >= 0, (entry.first + "::s < 0").c_str(), s0 = 0);

                entry.second.s0_to_poly[s0] = Poly3(s0, a, b, c, d);
            }
        }

        /* parse crossfall - has extra attribute side */
        if (with_lateral_profile)
        {
            for (pugi::xml_node crossfall_node : road_node.child("lateralProfile").children("crossfall"))
            {
                double s0 = crossfall_node.attribute("s").as_double(0.0);
                double a = crossfall_node.attribute("a").as_double(0.0);
                double b = crossfall_node.attribute("b").as_double(0.0);
                double c = crossfall_node.attribute("c").as_double(0.0);
                double d = crossfall_node.attribute("d").as_double(0.0);

                CHECK_AND_REPAIR(s0 >= 0, "road::lateralProfile::crossfall::s < 0", s0 = 0);

                Poly3 crossfall_poly(s0, a, b, c, d);
                road.crossfall.s0_to_poly[s0] = crossfall_poly;
                if (pugi::xml_attribute side = crossfall_node.attribute("side"))
                {
                    string side_str = side.as_string("");
                    transform(side_str.begin(), side_str.end(), side_str.begin(), [](unsigned char c) { return tolower(c); });
                    if (side_str == "left")
                        road.crossfall.sides[s0] = Crossfall::Side_Left;
                    else if (side_str == "right")
                        road.crossfall.sides[s0] = Crossfall::Side_Right;
                    else
                        road.crossfall.sides[s0] = Crossfall::Side_Both;
                }
            }

            /* check for lateralProfile shape - not implemented yet */
            // if (road_node.child("lateralProfile").child("shape"))
            // {
            //     printf("Lateral Profile Shape not supported\n");
            // }
        }

        /* parse road lane sections and lanes */
        for (pugi::xml_node lanesection_node : road_node.child("lanes").children("laneSection"))
        {
            const double s0 = lanesection_node.attribute("s").as_double(0.0);
            LaneSection& lanesection = road.s_to_lanesection.insert({s0, LaneSection(road_id, s0)}).first->second;
            lanesection.xml_node = lanesection_node;

            for (pugi::xpath_node lane_xpath_node : lanesection_node.select_nodes(".//lane"))
            {
                pugi::xml_node lane_node = lane_xpath_node.node();
                const int      lane_id = lane_node.attribute("id").as_int(0);

                Lane& lane =
                    lanesection.id_to_lane
                        .insert({lane_id,
                                 Lane(road_id, s0, lane_id, lane_node.attribute("level").as_bool(false), lane_node.attribute("type").as_string(""))})
                        .first->second;

                if (pugi::xml_node node = lane_node.child("link").child("predecessor"))
                    lane.predecessor = node.attribute("id").as_int(0);
                if (pugi::xml_node node = lane_node.child("link").child("successor"))
                    lane.successor = node.attribute("id").as_int(0);
                lane.xml_node = lane_node;

                for (pugi::xml_node lane_width_node : lane_node.children("width"))
                {
                    double s_offset = lane_width_node.attribute("sOffset").as_double(0.0);
                    double a = lane_width_node.attribute("a").as_double(0.0);
                    double b = lane_width_node.attribute("b").as_double(0.0);
                    double c = lane_width_node.attribute("c").as_double(0.0);
                    double d = lane_width_node.attribute("d").as_double(0.0);

                    CHECK_AND_REPAIR(s_offset >= 0, "lane::width::sOffset < 0", s_offset = 0);
                    lane.lane_width.s0_to_poly[s0 + s_offset] = Poly3(s0 + s_offset, a, b, c, d);
                }

                if (with_lane_height)
                {
                    for (pugi::xml_node lane_height_node : lane_node.children("height"))
                    {
                        double s_offset = lane_height_node.attribute("sOffset").as_double(0.0);
                        double inner = lane_height_node.attribute("inner").as_double(0.0);
                        double outer = lane_height_node.attribute("outer").as_double(0.0);

                        CHECK_AND_REPAIR(s_offset >= 0, "lane::height::sOffset < 0", s_offset = 0);
                        lane.s_to_height_offset.insert({s0 + s_offset, HeightOffset(inner, outer)});
                    }
                }

                for (pugi::xml_node roadmark_node : lane_node.children("roadMark"))
                {
                    RoadMarkGroup roadmark_group(road_id,
                                                 s0,
                                                 lane_id,
                                                 roadmark_node.attribute("width").as_double(-1),
                                                 roadmark_node.attribute("height").as_double(0),
                                                 roadmark_node.attribute("sOffset").as_double(0),
                                                 roadmark_node.attribute("type").as_string("none"),
                                                 roadmark_node.attribute("weight").as_string("standard"),
                                                 roadmark_node.attribute("color").as_string("standard"),
                                                 roadmark_node.attribute("material").as_string("standard"),
                                                 roadmark_node.attribute("laneChange").as_string("both"));
                    roadmark_group.xml_node = roadmark_node;

                    CHECK_AND_REPAIR(roadmark_group.s_offset >= 0, "lane::roadMark::sOffset < 0", roadmark_group.s_offset = 0);
                    const double roadmark_group_s0 = s0 + roadmark_group.s_offset;

                    if (pugi::xml_node roadmark_type_node = roadmark_node.child("type"))
                    {
                        const string name = roadmark_type_node.attribute("name").as_string("");
                        const double      line_width_1 = roadmark_type_node.attribute("width").as_double(-1);

                        for (pugi::xml_node roadmarks_line_node : roadmark_type_node.children("line"))
                        {
                            const double line_width_0 = roadmarks_line_node.attribute("width").as_double(-1);
                            const double roadmark_width = line_width_0 < 0 ? line_width_1 : line_width_0;

                            RoadMarksLine roadmarks_line(road_id,
                                                         s0,
                                                         lane_id,
                                                         roadmark_group_s0,
                                                         roadmark_width,
                                                         roadmarks_line_node.attribute("length").as_double(0),
                                                         roadmarks_line_node.attribute("space").as_double(0),
                                                         roadmarks_line_node.attribute("tOffset").as_double(0),
                                                         roadmarks_line_node.attribute("sOffset").as_double(0),
                                                         name,
                                                         roadmarks_line_node.attribute("rule").as_string("none"));
                            roadmarks_line.xml_node = roadmarks_line_node;

                            CHECK_AND_REPAIR(roadmarks_line.length >= 0, "roadMark::type::line::length < 0", roadmarks_line.length = 0);
                            CHECK_AND_REPAIR(roadmarks_line.space >= 0, "roadMark::type::line::space < 0", roadmarks_line.space = 0);
                            CHECK_AND_REPAIR(roadmarks_line.s_offset >= 0, "roadMark::type::line::sOffset < 0", roadmarks_line.s_offset = 0);

                            roadmark_group.roadmark_lines.emplace(move(roadmarks_line));
                        }
                    }

                    lane.roadmark_groups.emplace(move(roadmark_group));
                }
            }

            /* derive lane borders from lane widths */
            auto id_lane_iter0 = lanesection.id_to_lane.find(0);
            if (id_lane_iter0 == lanesection.id_to_lane.end())
                throw runtime_error("lane section does not have lane #0");

            /* iterate from id #0 towards +inf */
            auto id_lane_iter1 = next(id_lane_iter0);
            for (auto iter = id_lane_iter1; iter != lanesection.id_to_lane.end(); iter++)
            {
                if (iter == id_lane_iter0)
                {
                    iter->second.outer_border = iter->second.lane_width;
                }
                else
                {
                    iter->second.inner_border = prev(iter)->second.outer_border;
                    iter->second.outer_border = prev(iter)->second.outer_border.add(iter->second.lane_width);
                }
            }

            /* iterate from id #0 towards -inf */
            map<int, Lane>::reverse_iterator r_id_lane_iter_1(id_lane_iter0);
            for (auto r_iter = r_id_lane_iter_1; r_iter != lanesection.id_to_lane.rend(); r_iter++)
            {
                if (r_iter == r_id_lane_iter_1)
                {
                    r_iter->second.outer_border = r_iter->second.lane_width.negate();
                }
                else
                {
                    r_iter->second.inner_border = prev(r_iter)->second.outer_border;
                    r_iter->second.outer_border = prev(r_iter)->second.outer_border.add(r_iter->second.lane_width.negate());
                }
            }

            for (auto& id_lane : lanesection.id_to_lane)
            {
                id_lane.second.inner_border = id_lane.second.inner_border.add(road.lane_offset);
                id_lane.second.outer_border = id_lane.second.outer_border.add(road.lane_offset);
            }
        }

        /* parse road objects */
        if (with_road_objects)
        {
            const RoadObjectCorner::Type default_local_outline_type =
                abs_z_for_for_local_road_obj_outline ? RoadObjectCorner::Type_Local_AbsZ : RoadObjectCorner::Type_Local_RelZ;

            for (pugi::xml_node object_node : road_node.child("objects").children("object"))
            {
                string road_object_id = object_node.attribute("id").as_string("");
                CHECK_AND_REPAIR(road.id_to_object.find(road_object_id) == road.id_to_object.end(),
                                 (string("object::id already exists - ") + road_object_id).c_str(),
                                 road_object_id = road_object_id + string("_dup"));

                const bool  is_dynamic_object = string(object_node.attribute("dynamic").as_string("no")) == "yes" ? true : false;
                RoadObject& road_object = road.id_to_object
                                              .insert({road_object_id,
                                                       RoadObject(road_id,
                                                                  road_object_id,
                                                                  object_node.attribute("s").as_double(0),
                                                                  object_node.attribute("t").as_double(0),
                                                                  object_node.attribute("zOffset").as_double(0),
                                                                  object_node.attribute("length").as_double(0),
                                                                  object_node.attribute("validLength").as_double(0),
                                                                  object_node.attribute("width").as_double(0),
                                                                  object_node.attribute("radius").as_double(0),
                                                                  object_node.attribute("height").as_double(0),
                                                                  object_node.attribute("hdg").as_double(0),
                                                                  object_node.attribute("pitch").as_double(0),
                                                                  object_node.attribute("roll").as_double(0),
                                                                  object_node.attribute("type").as_string(""),
                                                                  object_node.attribute("name").as_string(""),
                                                                  object_node.attribute("orientation").as_string(""),
                                                                  object_node.attribute("subtype").as_string(""),
                                                                  is_dynamic_object)})
                                              .first->second;
                road_object.xml_node = object_node;

                CHECK_AND_REPAIR(road_object.s0 >= 0, "object::s < 0", road_object.s0 = 0);
                CHECK_AND_REPAIR(road_object.valid_length >= 0, "object::validLength < 0", road_object.valid_length = 0);
                CHECK_AND_REPAIR(road_object.length >= 0, "object::length < 0", road_object.length = 0);
                CHECK_AND_REPAIR(road_object.width >= 0, "object::width < 0", road_object.width = 0);
                CHECK_AND_REPAIR(road_object.radius >= 0, "object::radius < 0", road_object.radius = 0);

                for (pugi::xml_node repeat_node : object_node.children("repeat"))
                {
                    RoadObjectRepeat road_object_repeat(repeat_node.attribute("s").as_double(NAN),
                                                        repeat_node.attribute("length").as_double(0),
                                                        repeat_node.attribute("distance").as_double(0),
                                                        repeat_node.attribute("tStart").as_double(NAN),
                                                        repeat_node.attribute("tEnd").as_double(NAN),
                                                        repeat_node.attribute("widthStart").as_double(NAN),
                                                        repeat_node.attribute("widthEnd").as_double(NAN),
                                                        repeat_node.attribute("heightStart").as_double(NAN),
                                                        repeat_node.attribute("heightEnd").as_double(NAN),
                                                        repeat_node.attribute("zOffsetStart").as_double(NAN),
                                                        repeat_node.attribute("zOffsetEnd").as_double(NAN));
                    road_object_repeat.xml_node = repeat_node;

                    CHECK_AND_REPAIR(
                        isnan(road_object_repeat.s0) || road_object_repeat.s0 >= 0, "object::repeat::s < 0", road_object_repeat.s0 = 0);
                    CHECK_AND_REPAIR(isnan(road_object_repeat.width_start) || road_object_repeat.width_start >= 0,
                                     "object::repeat::widthStart < 0",
                                     road_object_repeat.width_start = 0);
                    CHECK_AND_REPAIR(isnan(road_object_repeat.width_end) || road_object_repeat.width_end >= 0,
                                     "object::repeat::widthStart < 0",
                                     road_object_repeat.width_end = 0);
                    CHECK_AND_REPAIR(road_object_repeat.length >= 0, "object::repeat::length < 0", road_object_repeat.length = 0);
                    CHECK_AND_REPAIR(road_object_repeat.distance >= 0, "object::repeat::distance < 0", road_object_repeat.distance = 0);

                    road_object.repeats.push_back(road_object_repeat);
                }

                /* since v1.45 multiple <outline> are allowed and parent tag is <outlines>, not <object>; this supports v1.4 and v1.45+ */
                pugi::xml_node outlines_parent_node = object_node.child("outlines") ? object_node.child("outlines") : object_node;
                for (pugi::xml_node outline_node : outlines_parent_node.children("outline"))
                {
                    RoadObjectOutline road_object_outline(outline_node.attribute("id").as_int(-1),
                                                          outline_node.attribute("fillType").as_string(""),
                                                          outline_node.attribute("laneType").as_string(""),
                                                          outline_node.attribute("outer").as_bool(true),
                                                          outline_node.attribute("closed").as_bool(true));
                    road_object_outline.xml_node = outline_node;

                    for (pugi::xml_node corner_local_node : outline_node.children("cornerLocal"))
                    {
                        const Vec3D pt_local{corner_local_node.attribute("u").as_double(0),
                                             corner_local_node.attribute("v").as_double(0),
                                             corner_local_node.attribute("z").as_double(0)};

                        RoadObjectCorner road_object_corner_local(corner_local_node.attribute("id").as_int(-1),
                                                                  pt_local,
                                                                  corner_local_node.attribute("height").as_double(0),
                                                                  default_local_outline_type);
                        road_object_corner_local.xml_node = corner_local_node;
                        road_object_outline.outline.push_back(road_object_corner_local);
                    }

                    for (pugi::xml_node corner_road_node : outline_node.children("cornerRoad"))
                    {
                        const Vec3D pt_road{corner_road_node.attribute("s").as_double(0),
                                            corner_road_node.attribute("t").as_double(0),
                                            corner_road_node.attribute("dz").as_double(0)};

                        RoadObjectCorner road_object_corner_road(corner_road_node.attribute("id").as_int(-1),
                                                                 pt_road,
                                                                 corner_road_node.attribute("height").as_double(0),
                                                                 RoadObjectCorner::Type_Road);
                        road_object_corner_road.xml_node = corner_road_node;
                        road_object_outline.outline.push_back(road_object_corner_road);
                    }

                    road_object.outlines.push_back(road_object_outline);
                }

                road_object.lane_validities = extract_lane_validity_records(object_node);
            }
        }
        /* parse signals */
        if (with_road_signals)
        {
            for (pugi::xml_node signal_node : road_node.child("signals").children("signal"))
            {
                string road_signal_id = signal_node.attribute("id").as_string("");
                CHECK_AND_REPAIR(road.id_to_signal.find(road_signal_id) == road.id_to_signal.end(),
                                 (string("signal::id already exists - ") + road_signal_id).c_str(),
                                 road_signal_id = road_signal_id + string("_dup"));

                RoadSignal& road_signal = road.id_to_signal
                                              .insert({road_signal_id,
                                                       RoadSignal(road_id,
                                                                  road_signal_id,
                                                                  signal_node.attribute("name").as_string(""),
                                                                  signal_node.attribute("s").as_double(0),
                                                                  signal_node.attribute("t").as_double(0),
                                                                  signal_node.attribute("dynamic").as_bool(),
                                                                  signal_node.attribute("zOffset").as_double(0),
                                                                  signal_node.attribute("value").as_double(0),
                                                                  signal_node.attribute("height").as_double(0),
                                                                  signal_node.attribute("width").as_double(0),
                                                                  signal_node.attribute("hOffset").as_double(0),
                                                                  signal_node.attribute("pitch").as_double(0),
                                                                  signal_node.attribute("roll").as_double(0),
                                                                  signal_node.attribute("orientation").as_string("none"),
                                                                  signal_node.attribute("country").as_string(""),
                                                                  signal_node.attribute("type").as_string("none"),
                                                                  signal_node.attribute("subtype").as_string("none"),
                                                                  signal_node.attribute("unit").as_string(""),
                                                                  signal_node.attribute("text").as_string("none"))})
                                              .first->second;
                road_signal.xml_node = signal_node;

                CHECK_AND_REPAIR(road_signal.s0 >= 0, "signal::s < 0", road_signal.s0 = 0);
                CHECK_AND_REPAIR(road_signal.height >= 0, "signal::height < 0", road_signal.height = 0);
                CHECK_AND_REPAIR(road_signal.width >= 0, "signal::width < 0", road_signal.width = 0);

                road_signal.lane_validities = extract_lane_validity_records(signal_node);
            }
        }
    }
    std::cout << "Parsing done" << std::endl;
    
    this->routing_graph = this->get_routing_graph();
    std::cout << "Routing graph done" << std::endl;

    this->connect_lanechanges();
    std::cout << "Connect lanechanges done" << std::endl;

    // Tests
    // {
    //     auto road = id_to_road.at("104");
    //     auto lanesection = road.get_lanesection(0.0);
    //     auto lane = lanesection.id_to_lane.find(1)->second;
    //     std::cout << "pos: " << road.get_surface_pt(0.0, 0.0)[0] << ", " << road.get_surface_pt(0.0, 0.0)[1] << std::endl;
    //     auto lane_mesh = road.get_lane_mesh(lane, 0.1);
    //     for (double i = 0; i < 100; i++)
    //     {
    //         auto geom = road.ref_line.get_geometry(i);
    //         std::cout << "geoms: " << geom->s0 << ", " << geom->x0 << ", " << geom->y0 << ", " << geom->hdg0 << ", " << geom->length << std::endl;
    //     }
    // }
}

std::vector<std::tuple<LaneKey, LaneKey, LaneKey>> OpenDriveMap::get_junction_connections(const JunctionConnection& conn, const std::string& id_junc) const
{
    vector<std::tuple<LaneKey, LaneKey, LaneKey>> result;

    auto incoming_road_iter = this->id_to_road.find(conn.incoming_road);
    auto connecting_road_iter = this->id_to_road.find(conn.connecting_road);
    if (incoming_road_iter == this->id_to_road.end() || connecting_road_iter == this->id_to_road.end())
        return result;
        
    const Road& incoming_road = incoming_road_iter->second;
    const Road& connecting_road = connecting_road_iter->second;

    auto next_road_id = (conn.contact_point == JunctionConnection::ContactPoint_Start)
                                                ? connecting_road.successor.id
                                                : connecting_road.predecessor.id;

    auto pre_road_id = (conn.contact_point == JunctionConnection::ContactPoint_Start)
                                                ? connecting_road.predecessor.id
                                                : connecting_road.successor.id;

    const bool is_succ_junc = incoming_road.successor.type == RoadLink::Type_Junction && incoming_road.successor.id == id_junc;
    const bool is_pred_junc = incoming_road.predecessor.type == RoadLink::Type_Junction && incoming_road.predecessor.id == id_junc;
    if (!is_succ_junc && !is_pred_junc)
        return result;

    const LaneSection& incoming_lanesec = is_pred_junc ? incoming_road.s_to_lanesection.begin()->second
                                                        : incoming_road.s_to_lanesection.rbegin()->second;

    const LaneSection& connecting_lanesec = (conn.contact_point == JunctionConnection::ContactPoint_Start)
                                                ? connecting_road.s_to_lanesection.begin()->second
                                                : connecting_road.s_to_lanesection.rbegin()->second;

    for (const JunctionLaneLink& lane_link : conn.lane_links)
    {
        if (lane_link.from == 0 || lane_link.to == 0)
            continue;

        auto from_lane_iter = incoming_lanesec.id_to_lane.find(lane_link.from);
        auto to_lane_iter = connecting_lanesec.id_to_lane.find(lane_link.to);

        if (from_lane_iter == incoming_lanesec.id_to_lane.end() || to_lane_iter == connecting_lanesec.id_to_lane.end())
            continue;

        const Lane& from_lane = from_lane_iter->second;
        const Lane& to_lane = to_lane_iter->second;

        if (from_lane.type != "driving" || to_lane.type != "driving" || from_lane.id == 0 || to_lane.id == 0)
            continue;

        bool lane_follows_road_direction = to_lane.id < 0;
        int next_lane_id = lane_follows_road_direction ? to_lane.successor : to_lane.predecessor;
        
        LaneKey next_lane_key(next_road_id, 0.0, next_lane_id);
        LaneKey pre_lane_key(pre_road_id, 0.0, from_lane.id);
        result.push_back(std::make_tuple(to_lane.key, next_lane_key, pre_lane_key));
    }

    return result;
}

void OpenDriveMap::connect_lanechanges()
{
    // roads
    for (auto road_iter = this->id_to_road.begin(); road_iter != this->id_to_road.end(); road_iter++)
    {
        for (auto lanesec_iter = road_iter->second.s_to_lanesection.begin(); lanesec_iter != road_iter->second.s_to_lanesection.end(); lanesec_iter++)
        {
            for (auto lane_iter = lanesec_iter->second.id_to_lane.begin(); lane_iter != lanesec_iter->second.id_to_lane.end(); lane_iter++)
            {
                if (lane_iter->second.id == 0 || lane_iter->second.type != "driving")
                    continue;

                auto adjacents = find_lane_adjacents(lane_iter->second);
                //std::cout << "lane adjacent (" << lane_iter->second.key.road_id << ", " << lane_iter->second.key.lanesection_s0 << ", " << lane_iter->second.key.lane_id << ")" << std::endl;
                // add to graph
                if (adjacents.first.road_id != "")
                {
                    lane_iter->second.right_lane = adjacents.first;
                    //std::cout << "right returned (" << adjacents.first.road_id << ", " << adjacents.first.lanesection_s0 << ", " << adjacents.first.lane_id << ")" << std::endl;
                    //std::cout << "right (" << lane_iter->second.right_lane.road_id << ", " << lane_iter->second.right_lane.lanesection_s0 << ", " << lane_iter->second.right_lane.lane_id << ")" << std::endl;
                }
                if (adjacents.second.road_id != "")
                {
                    lane_iter->second.left_lane = adjacents.second;
                    //std::cout << "left returned (" << adjacents.second.road_id << ", " << adjacents.second.lanesection_s0 << ", " << adjacents.second.lane_id << ")" << std::endl;
                    //std::cout << "left (" << lane_iter->second.left_lane.road_id << ", " << lane_iter->second.left_lane.lanesection_s0 << ", " << lane_iter->second.left_lane.lane_id << ")" << std::endl;
                }
            }
        }
    }
    // junctions
    for (const auto& id_junc : this->id_to_junction)
    {
        //std::cout << "==================================================================================================" << std::endl;

        for (const auto& id_conn : id_junc.second.id_to_connection)
        {
            auto conns = get_junction_connections(id_conn.second, id_junc.first);
            for (const auto& conn : conns)
            {
                const LaneKey& current_lane = std::get<0>(conn);
                const LaneKey& current_lane_next = std::get<1>(conn);
                const LaneKey& current_lane_pre = std::get<2>(conn);
                
                //std::cout << "---------------------------------------------------------------------------------------------------" << std::endl;
                //std::cout << "current_lane (" << current_lane.road_id << "," << current_lane.lanesection_s0 << "," << current_lane.lane_id << ")" << std::endl;
                //std::cout << "current_lane_next (" << current_lane_next.road_id << "," << current_lane_next.lanesection_s0 << "," << current_lane_next.lane_id << ")" << std::endl;
                //std::cout << "current_lane_pre (" << current_lane_pre.road_id << "," << current_lane_pre.lanesection_s0 << "," << current_lane_pre.lane_id << ")" << std::endl;

                for (const auto& id_conn_other : id_junc.second.id_to_connection)
                {
                    auto conns_other = get_junction_connections(id_conn_other.second, id_junc.first);
                    for (const auto& conn_other : conns_other)
                    {
                        const LaneKey& other_lane = std::get<0>(conn_other);
                        const LaneKey& other_lane_next = std::get<1>(conn_other);
                        const LaneKey& other_lane_pre = std::get<2>(conn_other);

                        //std::cout << "other_lane (" << other_lane.road_id << "," << other_lane.lanesection_s0 << "," << other_lane.lane_id << ")" << std::endl;
                        //std::cout << "other_lane_next (" << other_lane_next.road_id << "," << other_lane_next.lanesection_s0 << "," << other_lane_next.lane_id << ")" << std::endl;
                        //std::cout << "other_lane_pre (" << other_lane_pre.road_id << "," << other_lane_pre.lanesection_s0 << "," << other_lane_pre.lane_id << ")" << std::endl;

                        if (current_lane_next.road_id != other_lane_next.road_id || current_lane_pre.road_id != other_lane_pre.road_id)
                            continue;

                        bool current_lane_next_follows_road_dir = current_lane_next.lane_id < 0;
                        int next_lanes_diff = current_lane_next.lane_id - other_lane_next.lane_id;
                        int pre_lanes_diff = current_lane_pre.lane_id - other_lane_pre.lane_id;

                        //std::cout << "next_lanes_diff: " << next_lanes_diff << " pre_lanes_diff: " << pre_lanes_diff << std::endl;

                        // roads are next to each other
                        if (std::abs(next_lanes_diff) != 1 || std::abs(pre_lanes_diff) != 1)
                            continue;

                        // right side
                        if ((current_lane_next_follows_road_dir && next_lanes_diff == 1) || (!current_lane_next_follows_road_dir && next_lanes_diff == -1))
                        {
                            // get to_lane again as reference to change the value
                            auto road = this->id_to_road.find(current_lane.road_id);
                            auto lanesec = road->second.s_to_lanesection.find(current_lane.lanesection_s0);
                            auto lane = lanesec->second.id_to_lane.find(current_lane.lane_id);

                            lane->second.right_lane = other_lane;
                            //std::cout << "right (" << other_lane.road_id << "," << other_lane.lanesection_s0 << "," << other_lane.lane_id << ")" << std::endl;
                        }
                        // left side
                        else if ((current_lane_next_follows_road_dir && next_lanes_diff == -1) || (!current_lane_next_follows_road_dir && next_lanes_diff == 1))
                        {
                            // get to_lane again as reference to change the value
                            auto road = this->id_to_road.find(current_lane.road_id);
                            auto lanesec = road->second.s_to_lanesection.find(current_lane.lanesection_s0);
                            auto lane = lanesec->second.id_to_lane.find(current_lane.lane_id);

                            lane->second.left_lane = other_lane;
                            //std::cout << "left (" << other_lane.road_id << "," << other_lane.lanesection_s0 << "," << other_lane.lane_id << ")" << std::endl;
                        }
                    }
                }
            }
        }
    }
}

vector<Road> OpenDriveMap::get_roads() const { return get_map_values(this->id_to_road); }

vector<Junction> OpenDriveMap::get_junctions() const { return get_map_values(this->id_to_junction); }

std::vector<std::vector<LaneKey>> OpenDriveMap::shortest_path(const LaneKey& start, const LaneKey& end) const
{
    auto path = this->routing_graph.shortest_path(start, end);
    std::vector<std::vector<LaneKey>> new_path;

    if (path.size() == 0)
    {
        std::cout << "Path empty" << std::endl;
        return new_path;
    }

    for (auto i : path)
        std::cout << "path: " << i << std::endl;

    if (path.size() == 1)
    {
        new_path.push_back(std::vector<LaneKey>());
        new_path[new_path.size() - 1].push_back(path[0]);
        return new_path;
    }

    LaneKey lane_key = path[path.size() - 1];

    new_path.push_back(std::vector<LaneKey>());
    new_path[new_path.size() - 1].push_back(lane_key);

    auto right_lanes = this->get_right_lanes(lane_key);
    new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), right_lanes.begin(), right_lanes.end());
    auto left_lanes = this->get_left_lanes(lane_key);
    new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), left_lanes.begin(), left_lanes.end());

    for (size_t i = path.size() - 2; i > 0; --i)
    {
        if (path[i].lane_change(path[i + 1]))
            continue;

        const Road& road = this->id_to_road.find(lane_key.road_id)->second;
        const LaneSection& lanesec = road.get_lanesection(lane_key.lanesection_s0);
        const Lane& lane = lanesec.id_to_lane.find(lane_key.lane_id)->second;

        const Road& current_road = this->id_to_road.find(path[i].road_id)->second;
        const LaneSection& current_lanesec = current_road.get_lanesection(path[i].lanesection_s0);
        const Lane& current_lane = current_lanesec.id_to_lane.find(path[i].lane_id)->second;

        if (!current_lane.has_lanechange())
        {
            lane_key = current_lane.key;
            new_path.push_back(std::vector<LaneKey>());
            new_path[new_path.size() - 1].push_back(current_lane.key);
            continue;
        }

        const std::vector<LaneKey> predecessors = this->find_lane_predecessors(lane);
        if (predecessors.size() == 0)
        {
            new_path.push_back(std::vector<LaneKey>());
            new_path[new_path.size() - 1].push_back(current_lane.key);

            auto right_lanes = this->get_right_lanes(current_lane.key);
            new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), right_lanes.begin(), right_lanes.end());
            auto left_lanes = this->get_left_lanes(current_lane.key);
            new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), left_lanes.begin(), left_lanes.end());

            lane_key = current_lane.key;
            continue;
        }
        if (predecessors.size() == 1)
        {
            const Road& temp_road = this->id_to_road.find(predecessors[0].road_id)->second;
            const LaneSection& temp_lanesec = temp_road.get_lanesection(predecessors[0].lanesection_s0);
            const Lane& temp_lane = temp_lanesec.id_to_lane.find(predecessors[0].lane_id)->second;

            if (!temp_lane.has_lanechange())
            {
                new_path.push_back(std::vector<LaneKey>());
                new_path[new_path.size() - 1].push_back(current_lane.key);

                auto right_lanes = this->get_right_lanes(current_lane.key);
                new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), right_lanes.begin(), right_lanes.end());
                auto left_lanes = this->get_left_lanes(current_lane.key);
                new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), left_lanes.begin(), left_lanes.end());

                lane_key = current_lane.key;
                continue;
            }

            new_path.push_back(std::vector<LaneKey>());
            new_path[new_path.size() - 1].push_back(predecessors[0]);

            auto right_lanes = this->get_right_lanes(predecessors[0]);
            new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), right_lanes.begin(), right_lanes.end());
            auto left_lanes = this->get_left_lanes(predecessors[0]);
            new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), left_lanes.begin(), left_lanes.end());

            lane_key = predecessors[0];
            continue;
        }

        for (auto pred : predecessors)
        {
            const Road& pred_road = this->id_to_road.find(pred.road_id)->second;
            const LaneSection& pred_lanesec = pred_road.get_lanesection(pred.lanesection_s0);
            const Lane& pred_lane = pred_lanesec.id_to_lane.find(pred.lane_id)->second;

            std::vector<LaneKey> pred_preds = this->find_lane_predecessors(pred_lane);

            if (path[i - 1].road_id == pred_preds[0].road_id)
            {
                new_path.push_back(std::vector<LaneKey>());
                new_path[new_path.size() - 1].push_back(pred);

                auto right_lanes = this->get_right_lanes(pred);
                new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), right_lanes.begin(), right_lanes.end());
                auto left_lanes = this->get_left_lanes(pred);
                new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), left_lanes.begin(), left_lanes.end());
                
                lane_key = pred;
                break;
            }
        }
    }

    new_path.push_back(std::vector<LaneKey>());
    new_path[new_path.size() - 1].push_back(path[0]);

    right_lanes = this->get_right_lanes(path[0]);
    new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), right_lanes.begin(), right_lanes.end());
    left_lanes = this->get_left_lanes(path[0]);
    new_path[new_path.size() - 1].insert(new_path[new_path.size() - 1].end(), left_lanes.begin(), left_lanes.end());

    std::reverse(new_path.begin(), new_path.end());
    for (auto i : new_path)
        for (auto j : i)
        std::cout << "new path: " << j << std::endl;

    return new_path;
}

std::vector<LaneKey> OpenDriveMap::get_right_lanes(const LaneKey& lane_key) const
{
    std::vector<LaneKey> right_lanes;

    // Add optional right lanes
    const Road& road = this->id_to_road.find(lane_key.road_id)->second;
    const LaneSection& lanesec = road.get_lanesection(lane_key.lanesection_s0);
    const Lane& lane = lanesec.id_to_lane.find(lane_key.lane_id)->second;
    LaneKey right_lane_key = lane.right_lane;

    std::cout << "lane: " << lane.key << ", right lane: " << right_lane_key << std::endl;

    while (right_lane_key.road_id != "")
    {
        right_lanes.push_back(right_lane_key);

        const Road& right_road = this->id_to_road.find(right_lane_key.road_id)->second;
        const LaneSection& right_lanesec = right_road.get_lanesection(right_lane_key.lanesection_s0);
        const Lane& right_lane = right_lanesec.id_to_lane.find(right_lane_key.lane_id)->second;
        right_lane_key = right_lane.right_lane;
    }

    return right_lanes;
}

std::vector<LaneKey> OpenDriveMap::get_left_lanes(const LaneKey& lane_key) const
{
    std::vector<LaneKey> left_lanes;

    // Add optional left lanes
    const Road& road = this->id_to_road.find(lane_key.road_id)->second;
    const LaneSection& lanesec = road.get_lanesection(lane_key.lanesection_s0);
    const Lane& lane = lanesec.id_to_lane.find(lane_key.lane_id)->second;
    LaneKey left_lane_key = lane.left_lane;

    std::cout << "lane: " << lane.key << ", left lane: " << left_lane_key << std::endl;

    while (left_lane_key.road_id != "")
    {
        left_lanes.push_back(left_lane_key);

        const Road& left_road = this->id_to_road.find(left_lane_key.road_id)->second;
        const LaneSection& left_lanesec = left_road.get_lanesection(left_lane_key.lanesection_s0);
        const Lane& left_lane = left_lanesec.id_to_lane.find(left_lane_key.lane_id)->second;
        left_lane_key = left_lane.left_lane;
    }

    return left_lanes;
}

RoadNetworkMesh OpenDriveMap::get_road_network_mesh(const double eps) const
{
    RoadNetworkMesh  out_mesh;
    LanesMesh&       lanes_mesh = out_mesh.lanes_mesh;
    RoadmarksMesh&   roadmarks_mesh = out_mesh.roadmarks_mesh;
    RoadObjectsMesh& road_objects_mesh = out_mesh.road_objects_mesh;
    RoadSignalsMesh& road_signals_mesh = out_mesh.road_signals_mesh;

    for (const auto& id_road : this->id_to_road)
    {
        const Road& road = id_road.second;
        lanes_mesh.road_start_indices[lanes_mesh.vertices.size()] = road.id;
        roadmarks_mesh.road_start_indices[roadmarks_mesh.vertices.size()] = road.id;
        road_objects_mesh.road_start_indices[road_objects_mesh.vertices.size()] = road.id;

        for (const auto& s_lanesec : road.s_to_lanesection)
        {
            const LaneSection& lanesec = s_lanesec.second;
            lanes_mesh.lanesec_start_indices[lanes_mesh.vertices.size()] = lanesec.s0;
            roadmarks_mesh.lanesec_start_indices[roadmarks_mesh.vertices.size()] = lanesec.s0;
            for (const auto& id_lane : lanesec.id_to_lane)
            {
                const Lane& lane = id_lane.second;

                // std::cout << "lane: " << lane.key << std::endl;

                const size_t lanes_idx_offset = lanes_mesh.vertices.size();
                lanes_mesh.lane_start_indices[lanes_idx_offset] = lane.id;
                lanes_mesh.add_mesh(road.get_lane_mesh(lane, eps));

                // std::cout << "done" << std::endl;

                size_t roadmarks_idx_offset = roadmarks_mesh.vertices.size();
                roadmarks_mesh.lane_start_indices[roadmarks_idx_offset] = lane.id;
                const vector<RoadMark> roadmarks = lane.get_roadmarks(lanesec.s0, road.get_lanesection_end(lanesec));
                for (const RoadMark& roadmark : roadmarks)
                {
                    roadmarks_idx_offset = roadmarks_mesh.vertices.size();
                    roadmarks_mesh.roadmark_type_start_indices[roadmarks_idx_offset] = roadmark.type;
                    roadmarks_mesh.add_mesh(road.get_roadmark_mesh(lane, roadmark, eps));
                }
            }
        }

        for (const auto& id_road_object : road.id_to_object)
        {
            const RoadObject& road_object = id_road_object.second;
            const size_t road_objs_idx_offset = road_objects_mesh.vertices.size();
            road_objects_mesh.road_object_start_indices[road_objs_idx_offset] = road_object.id;
            road_objects_mesh.add_mesh(road.get_road_object_mesh(road_object, eps));
        }

        for (const auto& id_signal : road.id_to_signal)
        {
            const RoadSignal& road_signal = id_signal.second;
            const size_t signals_idx_offset = road_signals_mesh.vertices.size();
            road_signals_mesh.road_signal_start_indices[signals_idx_offset] = road_signal.id;
            road_signals_mesh.add_mesh(road.get_road_signal_mesh(road_signal));
        }
    }

    return out_mesh;
}

RoutingGraph OpenDriveMap::get_routing_graph() const
{
    RoutingGraph routing_graph;
    // for each road
    for (const auto& id_road : id_to_road)
    {
        // std::cout << "----------------------------------------------------------------------------" << std::endl;
        const Road& road = id_road.second;
        // for each lanesection
        for (auto s_lanesec_iter = road.s_to_lanesection.begin(); s_lanesec_iter != road.s_to_lanesection.end(); s_lanesec_iter++)
        {
            const LaneSection& lanesec = s_lanesec_iter->second;
            // for each lane
            for (const auto& id_lane : lanesec.id_to_lane)
            {
                const Lane& lane = id_lane.second;

                if (lane.type != "driving" || lane.id == 0)
                    continue;

                // get lane predecessors
                auto predecessors = find_lane_predecessors(lane);
                // add to graph

                // std::cout << "main lane: " << lane.key << std::endl;

                for (const auto& predecessor : predecessors)
                {
                    // std::cout << "predecessor lane: " << predecessor << std::endl;
                    double lane_length = road.get_lanesection_length(predecessor.lanesection_s0);
                    routing_graph.add_edge(RoutingGraphEdge(predecessor, lane.key, lane_length));
                }
                // get lane successors
                auto successors = find_lane_successors(lane);
                // add to graph
                for (const auto& successor : successors)
                {
                    double lane_length = road.get_lanesection_length(lane.key.lanesection_s0);
                    routing_graph.add_edge(RoutingGraphEdge(lane.key, successor, lane_length));
                }
                // get adjacent lanes
                auto adjacents = find_lane_adjacents(lane);
                // add to graph
                if (adjacents.first.road_id != "")
                    routing_graph.add_edge(RoutingGraphEdge(lane.key, adjacents.first, 1.0));
                if (adjacents.second.road_id != "")
                    routing_graph.add_edge(RoutingGraphEdge(lane.key, adjacents.second, 1.0));
            }
        }
    }
    /* parse junctions
    for (const auto& id_junc : this->id_to_junction)
    {
        for (const auto& id_conn : id_junc.second.id_to_connection)
        {
            const JunctionConnection& conn = id_conn.second;

            auto incoming_road_iter = this->id_to_road.find(conn.incoming_road);
            auto connecting_road_iter = this->id_to_road.find(conn.connecting_road);
            if (incoming_road_iter == this->id_to_road.end() || connecting_road_iter == this->id_to_road.end())
                continue;
                
            const Road& incoming_road = incoming_road_iter->second;
            const Road& connecting_road = connecting_road_iter->second;

            const bool is_succ_junc = incoming_road.successor.type == RoadLink::Type_Junction && incoming_road.successor.id == id_junc.first;
            const bool is_pred_junc = incoming_road.predecessor.type == RoadLink::Type_Junction && incoming_road.predecessor.id == id_junc.first;
            if (!is_succ_junc && !is_pred_junc)
                continue;

            const LaneSection& incoming_lanesec = is_pred_junc ? incoming_road.s_to_lanesection.begin()->second
                                                               : incoming_road.s_to_lanesection.rbegin()->second;

            const LaneSection& connecting_lanesec = (conn.contact_point == JunctionConnection::ContactPoint_Start)
                                                        ? connecting_road.s_to_lanesection.begin()->second
                                                        : connecting_road.s_to_lanesection.rbegin()->second;

            for (const JunctionLaneLink& lane_link : conn.lane_links)
            {
                if (lane_link.from == 0 || lane_link.to == 0)
                    continue;

                auto from_lane_iter = incoming_lanesec.id_to_lane.find(lane_link.from);
                auto to_lane_iter = connecting_lanesec.id_to_lane.find(lane_link.to);

                if (from_lane_iter == incoming_lanesec.id_to_lane.end() || to_lane_iter == connecting_lanesec.id_to_lane.end())
                    continue;

                const Lane& from_lane = from_lane_iter->second;
                const Lane& to_lane = to_lane_iter->second;

                if (from_lane.type != "driving" || to_lane.type != "driving")
                    continue;

                const LaneKey from(incoming_road.id, incoming_lanesec.s0, from_lane.id);
                const LaneKey to(connecting_road.id, connecting_lanesec.s0, to_lane.id);
                const double  lane_length = incoming_road.get_lanesection_length(incoming_lanesec);

                routing_graph.add_edge(RoutingGraphEdge(from, to, lane_length));
            }
        }
    }*/

    return routing_graph;
}

vector<LaneKey> OpenDriveMap::find_lane_predecessors(const Lane& lane) const
{
    vector<LaneKey> predecessor_lanes;
    bool lane_follows_road_direction = lane.id < 0; // ASSUMES NOT IN UK or other left lane drivers.

    auto current_road = id_to_road.at(lane.key.road_id);
    auto current_lanesection = current_road.get_lanesection(lane.key.lanesection_s0);

    auto prev_lanesection = lane_follows_road_direction ? current_road.get_previous_lanesection(current_lanesection)
                                                        : current_road.get_next_lanesection(current_lanesection);

    if (!prev_lanesection)
    {
        // if roadlinks at the correct end use those
        const RoadLink& road_link = lane_follows_road_direction ? current_road.predecessor : current_road.successor;
        if (road_link.type == RoadLink::Type_Road && road_link.contact_point != RoadLink::ContactPoint_None)
        {
            auto prev_road_iter = this->id_to_road.find(road_link.id);
            if (prev_road_iter != this->id_to_road.end())
            {
                const Road& prev_road = prev_road_iter->second;
                prev_lanesection = (road_link.contact_point == RoadLink::ContactPoint_Start) ? prev_road.s_to_lanesection.begin()->second
                                                                                             : prev_road.s_to_lanesection.rbegin()->second;
            }
        }
    }

    if (prev_lanesection)
    {
        // Check if the optional next_lanesection has a value
        const LaneSection& prev_lane_section_value = prev_lanesection.value();

        if (lane_follows_road_direction && lane.predecessor != 0)
        {
            // Check if lane.predecessor is a valid key before using at
            auto it = prev_lane_section_value.id_to_lane.find(lane.predecessor);
            if (it != prev_lane_section_value.id_to_lane.end())
                predecessor_lanes.push_back(it->second.key);
        }
        else if (!lane_follows_road_direction && lane.successor != 0)
        {
            // Check if lane.successor is a valid key before using at
            auto it = prev_lane_section_value.id_to_lane.find(lane.successor);
            if (it != prev_lane_section_value.id_to_lane.end())
                predecessor_lanes.push_back(it->second.key);
        }
    }

    // check junctions for road
    if (prev_lanesection)
        return predecessor_lanes;

    const RoadLink& road_link = lane_follows_road_direction ? current_road.predecessor : current_road.successor;
    if (road_link.type != RoadLink::Type_Junction)
        return predecessor_lanes;

    // std::cout << "----------------------------------------------------" << std::endl;
    // std::cout << "main lane: " << lane.key << std::endl;

    auto id_junc = this->id_to_junction.find(road_link.id)->second;
    for (const auto& id_conn : id_junc.id_to_connection)
    {
        const JunctionConnection& conn = id_conn.second;

        auto connecting_road_iter = this->id_to_road.find(conn.connecting_road);
        if (connecting_road_iter == this->id_to_road.end())
            continue;
            
        const Road& connecting_road = connecting_road_iter->second;

        for (const JunctionLaneLink& lane_link : conn.lane_links)
        {
            if (lane_link.to == 0)
                continue;

            bool conn_road_follows_lane_dir = lane_link.to < 0;

            const LaneSection& connecting_lanesec = conn_road_follows_lane_dir ? connecting_road.s_to_lanesection.rbegin()->second
                                                                               : connecting_road.s_to_lanesection.begin()->second;

            auto to_lane_iter = connecting_lanesec.id_to_lane.find(lane_link.to);

            if (to_lane_iter == connecting_lanesec.id_to_lane.end())
                continue;

            const Lane& to_lane = to_lane_iter->second;

            if (to_lane.type != "driving")
                continue;

            // std::cout << "to lane: " << to_lane.key << std::endl;

            bool add_conn = (conn_road_follows_lane_dir && connecting_road.successor.id == current_road.id && to_lane.successor == lane.id)
                         || (!conn_road_follows_lane_dir && connecting_road.predecessor.id == current_road.id && to_lane.predecessor == lane.id);

            if (!add_conn)
                continue;

            const LaneKey to(connecting_road.id, connecting_lanesec.s0, to_lane.id);
            // std::cout << "to lane added: " << to << std::endl;

            predecessor_lanes.push_back(to);
        }
    }    

    return predecessor_lanes;
}

vector<LaneKey> OpenDriveMap::find_lane_successors(const Lane& lane) const
{
    vector<LaneKey> successor_lanes;
    bool lane_follows_road_direction = lane.id < 0; // ASSUMES NOT IN UK or other left lane drivers.

    auto current_road = id_to_road.at(lane.key.road_id);
    auto current_lanesection = current_road.get_lanesection(lane.key.lanesection_s0);

    auto next_lanesection = lane_follows_road_direction ? current_road.get_next_lanesection(current_lanesection)
                                                        : current_road.get_previous_lanesection(current_lanesection);

    // if roadlinks at the correct end use those
    if (!next_lanesection)
    {
        const RoadLink& road_link = lane_follows_road_direction ? current_road.successor : current_road.predecessor;
        if (road_link.type == RoadLink::Type_Road && road_link.contact_point != RoadLink::ContactPoint_None)
        {
            auto next_road_iter = this->id_to_road.find(road_link.id);
            if (!(next_road_iter == this->id_to_road.end()))
            {
                const Road& next_road = next_road_iter->second;
                next_lanesection = (road_link.contact_point == RoadLink::ContactPoint_Start) ? next_road.s_to_lanesection.begin()->second
                                                                                             : next_road.s_to_lanesection.rbegin()->second;
            }
        }
    }

    if (next_lanesection)
    {
        // Check if the optional next_lanesection has a value
        const LaneSection& next_lane_section_value = next_lanesection.value();

        if (lane_follows_road_direction && lane.successor != 0)
        {
            // Check if lane.successor is a valid key before using at
            auto it = next_lane_section_value.id_to_lane.find(lane.successor);
            if (it != next_lane_section_value.id_to_lane.end())
                successor_lanes.push_back(it->second.key);
        }
        else if (!lane_follows_road_direction && lane.predecessor != 0)
        {
            // Check if lane.predecessor is a valid key before using at
            auto it = next_lane_section_value.id_to_lane.find(lane.predecessor);
            if (it != next_lane_section_value.id_to_lane.end())
                successor_lanes.push_back(it->second.key);
        }
    }

    // check junctions for road
    if (next_lanesection)
        return successor_lanes;

    const RoadLink& road_link = lane_follows_road_direction ? current_road.successor : current_road.predecessor;
    if (road_link.type != RoadLink::Type_Junction)
        return successor_lanes;

    // std::cout << "----------------------------------------------------" << std::endl;
    // std::cout << "main lane: " << lane.key << std::endl;

    auto id_junc = this->id_to_junction.find(road_link.id)->second;
    for (const auto& id_conn : id_junc.id_to_connection)
    {
        const JunctionConnection& conn = id_conn.second;

        auto connecting_road_iter = this->id_to_road.find(conn.connecting_road);
        if (connecting_road_iter == this->id_to_road.end())
            continue;
            
        const Road& connecting_road = connecting_road_iter->second;

        for (const JunctionLaneLink& lane_link : conn.lane_links)
        {
            if (lane_link.to == 0)
                continue;

            bool conn_road_follows_lane_dir = lane_link.to < 0;

            const LaneSection& connecting_lanesec = conn_road_follows_lane_dir ? connecting_road.s_to_lanesection.begin()->second
                                                                               : connecting_road.s_to_lanesection.rbegin()->second;

            auto to_lane_iter = connecting_lanesec.id_to_lane.find(lane_link.to);

            if (to_lane_iter == connecting_lanesec.id_to_lane.end())
                continue;

            const Lane& to_lane = to_lane_iter->second;

            if (to_lane.type != "driving")
                continue;

            // std::cout << "to lane: " << to_lane.key << std::endl;

            bool add_conn = (conn_road_follows_lane_dir && connecting_road.predecessor.id == current_road.id && to_lane.predecessor == lane.id)
                         || (!conn_road_follows_lane_dir && connecting_road.successor.id == current_road.id && to_lane.successor == lane.id);

            if (!add_conn)
                continue;

            const LaneKey to(connecting_road.id, connecting_lanesec.s0, to_lane.id);
            // std::cout << "to lane added: " << to << std::endl;

            successor_lanes.push_back(to);
        }
    }
    return successor_lanes;
}

std::vector<LaneKey> OpenDriveMap::get_junction_connections(const Lane& lane) const
{
    vector<LaneKey> connections;
    bool lane_follows_road_direction = lane.id < 0; // ASSUMES NOT IN UK or other left lane drivers.
    auto current_road = id_to_road.at(lane.key.road_id);

    if ((lane_follows_road_direction && current_road.predecessor.type == RoadLink::Type_Junction) ||
        (!lane_follows_road_direction && current_road.successor.type == RoadLink::Type_Junction))
    {
        const Junction& junction = lane_follows_road_direction ? id_to_junction.find(current_road.predecessor.id)->second
                                                               : id_to_junction.find(current_road.successor.id)->second;
        for (const auto& id_conn : junction.id_to_connection)
        {
            const JunctionConnection& conn = id_conn.second;

            auto incoming_road_iter = this->id_to_road.find(conn.incoming_road);
            auto connecting_road_iter = this->id_to_road.find(conn.connecting_road);
            if (incoming_road_iter == this->id_to_road.end() || connecting_road_iter == this->id_to_road.end())
                continue;
                
            const Road& incoming_road = incoming_road_iter->second;
            const Road& connecting_road = connecting_road_iter->second;

            if (incoming_road.id != current_road.id)
                continue;

            const bool is_succ_junc = incoming_road.successor.type == RoadLink::Type_Junction && incoming_road.successor.id == junction.id;
            const bool is_pred_junc = incoming_road.predecessor.type == RoadLink::Type_Junction && incoming_road.predecessor.id == junction.id;
            if (!is_succ_junc && !is_pred_junc)
                continue;

            const LaneSection& incoming_lanesec = is_pred_junc ? incoming_road.s_to_lanesection.begin()->second
                                                                : incoming_road.s_to_lanesection.rbegin()->second;

            const LaneSection& connecting_lanesec = (conn.contact_point == JunctionConnection::ContactPoint_Start)
                                                        ? connecting_road.s_to_lanesection.begin()->second
                                                        : connecting_road.s_to_lanesection.rbegin()->second;

            for (const JunctionLaneLink& lane_link : conn.lane_links)
            {
                if (lane_link.from == 0 || lane_link.to == 0)
                    continue;

                auto from_lane_iter = incoming_lanesec.id_to_lane.find(lane_link.from);
                auto to_lane_iter = connecting_lanesec.id_to_lane.find(lane_link.to);

                if (from_lane_iter == incoming_lanesec.id_to_lane.end() || to_lane_iter == connecting_lanesec.id_to_lane.end())
                    continue;

                const Lane& from_lane = from_lane_iter->second;
                const Lane& to_lane = to_lane_iter->second;

                if (from_lane.type != "driving" || to_lane.type != "driving")
                    continue;

                if (to_lane.id != lane.id)
                    continue;

                const LaneKey from(incoming_road.id, incoming_lanesec.s0, from_lane.id);
                const LaneKey to(connecting_road.id, connecting_lanesec.s0, to_lane.id);
                const double  lane_length = incoming_road.get_lanesection_length(incoming_lanesec);

                bool lane_follows_road_direction = from_lane.id < 0; // ASSUMES NOT IN UK or other left lane drivers.

                connections.push_back(from);
            }
        }
    }
}

std::pair<LaneKey, LaneKey> OpenDriveMap::find_lane_adjacents(const Lane& lane) const
{
    std::pair<LaneKey, LaneKey> adjacent_lanes;
    bool lane_follows_road_direction = lane.id < 0; // ASSUMES NOT IN UK or other left lane drivers.

    auto current_road = id_to_road.at(lane.key.road_id);
    auto current_lanesection = current_road.get_lanesection(lane.key.lanesection_s0);

    int right_lane_id = lane_follows_road_direction ? lane.id - 1 : lane.id + 1;
    auto right_lane_it = current_lanesection.id_to_lane.find(right_lane_id);

    //std::cout << "adjacent lanes (" << lane.key.road_id << ", " << lane.key.lanesection_s0 << ", " << lane.key.lane_id << ")" << std::endl;

    // right lane exists, not the center lane, and is on the same side of the road as the current lane
    if (right_lane_it != current_lanesection.id_to_lane.end() && right_lane_id != 0 && (lane.id<0) == (right_lane_id<0))
    {
        auto right_lane = right_lane_it->second;
        if (right_lane.type == "driving")
        {
            //std::cout << "adjacent lanes right (" << right_lane.key.road_id << ", " << right_lane.key.lanesection_s0 << ", " << right_lane.key.lane_id << ")" << std::endl;
            adjacent_lanes.first = right_lane.key;
        }
    }

    int left_lane_id = lane_follows_road_direction ? lane.id + 1 : lane.id - 1;
    auto left_lane_it = current_lanesection.id_to_lane.find(left_lane_id);

    // left lane exists, not the center lane, and is on the same side of the road as the current lane
    if (left_lane_it != current_lanesection.id_to_lane.end() && left_lane_id != 0 && (lane.id<0) == (left_lane_id<0))
    {
        auto left_lane = left_lane_it->second;
        if (left_lane.type == "driving")
        {
            //std::cout << "adjacent lanes left (" << left_lane.key.road_id << ", " << left_lane.key.lanesection_s0 << ", " << left_lane.key.lane_id << ")" << std::endl;
            adjacent_lanes.second = left_lane.key;
        }
    }

    return adjacent_lanes;
}

std::tuple<LaneKey, double> OpenDriveMap::find_lane(double x, double y, double eps) const
{
    auto start = std::chrono::high_resolution_clock::now();

    for (auto road : this->get_roads())
    {
        auto [lane_key, s] = road.find_lane(x, y, eps);
        if (s == -1)
            continue;

        // auto end = std::chrono::high_resolution_clock::now();

        // // Calculate duration
        // std::chrono::duration<double> duration = end - start;

        // // Output duration in seconds
        // std::cout << duration.count() << ",";

        return {lane_key, s};
    }

    throw runtime_error("Point not on any road.");
}

std::tuple<LaneKey, double> OpenDriveMap::match_lane(double x, double y, double eps) const
{
    auto start = std::chrono::high_resolution_clock::now();
    double closest_s = 100000000;
    std::tuple<LaneKey, double> closest_res;

    for (auto road : this->get_roads())
    {
        auto [lane_key, s] = road.match_lane(x, y, eps);
        if (s == -1)
            continue;

        if (s < closest_s) {
            closest_s = s;
            closest_res = {lane_key, s};
        }
        //return {lane_key, s};
    }

    // auto end = std::chrono::high_resolution_clock::now();

    // // Calculate duration
    // std::chrono::duration<double> duration = end - start;

    // // Output duration in seconds
    // std::cout << duration.count() << std::endl;

    return closest_res;
}

} // namespace odr
